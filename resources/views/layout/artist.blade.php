<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <title>Artrista {{Auth::user()->name}}</title>
    <style>
    .rede {
      text-align: center;
    }

    .rede i {
      font-size: 30px;
      color: #545454;
    }
    </style>
</head>
<body>
<nav class="navbar  navbar-expand-md navbar-light" style="background-color: #fff;">
<img src="/images/logo.png" alt="">
<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse"
  aria-expanded="false" aria-label="Toggle navigation">
  <span class="navbar-toggler-icon"></span>
</button>
<div class="collapse navbar-collapse justify-content-end" id="navbarCollapse">
  <ul class="navbar-nav">
  <li class="nav-item">
      <a class="nav-link" style="color:#b71912" href="/">A galeria</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="/home">Minhas obras</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="/exposicao/artista">Minhas exposicões</a>
    </li>
    <li class="nav-item">
          <a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
            Logout
          </a>
          <form class="nav-link" id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
          </form>
        </li>

  </ul>
</div>
</nav>

@yield('content')
<footer class="container">
    <hr class="featurette-divider">
    <p class="float-right">
      <a href="#">Volte ao inicio</a>
    </p>
    <p>&copy; 2017 Carlos Teixeira&Nuno Gonçalves
      <div class="rede">
        <a href="https://www.facebook.com/quadrasoltas" target="_blank">
          <i class="fa fa-facebook-square" aria-hidden="true" style="padding-right: 2%"></i>
        </a>
        <a href="https://www.instagram.com/quadrasoltas2010" target="_blank">
          <i class="fa fa-instagram" aria-hidden="true" style="padding-right: 2%"></i>
        </a>
        <a href="https://www.pinterest.pt/quadrasoltas/" target="_blank">
          <i class="fa fa-pinterest" aria-hidden="true"></i>
        </a>
      </div>
    </p>

  </footer>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
</body>
</html>