<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<head>
	<meta charset="utf-8"/>
	<title>@yield('title')</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="width=device-width, initial-scale=1" name="viewport"/>
	<meta content="" name="description"/>
	<meta content="" name="author"/>
	<link rel="stylesheet" href="{{ asset("assets/stylesheets/styles.css") }}" />
</head>
<body>
<div id="wrapper">
        <nav class="navbar navbar-static-top" role="navigation"  style="margin:0">
            <div class="navbar-header">
                <button type="button" style="background-color:gainsboro" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span style="background-color:#B61912" class="sr-only">Toggle navigation</span>
                    <span style="background-color:#B61912" class="icon-bar"></span>
                    <span style="background-color:#B61912" class="icon-bar"></span>
                    <span style="background-color:#B61912" class="icon-bar"></span>
                </button>
            <img style="padding:5% 0 10% 30%" src="/images/logo.png" alt="">
            </div>
            <!-- /.navbar-header -->
            <div class="navbar-default sidebar" role="navigation" style="margin-top:80px;">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        
                        <li >
                            <a href="/home"><i class="fa fa-dashboard fa-fw"></i> Painel Administrativo</a>
                        </li>
                        <li>
                            <a href="/comprasPendente"><i class="fa fa-money fa-fw"></i> Pedidos de compras</a>
                            <!-- /.nav-second-level -->
                        </li>
                        
                        <li >
                            <a href="#"><i class="fa fa-users fa-fw"></i> Users<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="/user">Listar todos</a>
                                </li>
                                <li >
                                    <a href="/pendente">Promover utilizadores</a>
                                </li>
                                
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-picture-o fa-fw"></i> Obras<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="/obras">Listar</a>
                                </li>
                                
                                <li>
                                    <a href="/obras/create">Adicionar</a>
                                </li>
                                
                                <li>
                                    <a href="/obras/pedidos">Aprovar</a>
                                </li>
                                
                                
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-sitemap fa-fw"></i> Exposições<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="/exposicao">Todas as exposicões</a>
                                </li>
                                <li>
                                    <a href="/exposicao/create">Adicionar</a>
                                </li>
                                <li>
                                    <a href="/exposicao/pedidos">Ver marcações</a>
                                </li>
                                
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="/contacte"><i class="fa fa-envelope fa-fw"></i> Mensagens Recebidas</a>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="fa fa-sign-out fa-fw"></i>
                                Logout
                            </a>
                            <form class="nav-link" id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>                      
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
			 <div class="row">
                <div class="col-lg-12">
                    <h2 class="page-header ">@yield('page_heading')</h2>
                </div>
                @yield('form')
                
                <!-- /.col-lg-12 -->
           </div>
			<div class="row">  
            @yield('content')
            </div>
            <!-- /#page-wrapper -->
        </div>
    </div>
	<script src="{{ asset("assets/scripts/frontend.js") }}" type="text/javascript"></script>
</body>
</html>