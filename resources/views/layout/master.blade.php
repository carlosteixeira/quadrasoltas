<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>@yield('title')</title>
  @yield('css')
  <style media="screen">
    .login-block {
      width: 320px;
      padding: 20px;
      background: #fff;
      margin: 0 auto;
    }

    .login-block h1 {
      text-align: center;
      color: #000;
      font-size: 18px;
      text-transform: uppercase;
      margin-top: 0;
      margin-bottom: 20px;
    }

    .login-block input {
      width: 100%;
      height: 42px;
      box-sizing: border-box;
      border-radius: 5px;
      border: 1px solid #ccc;
      margin-bottom: 20px;
      font-size: 14px;
      font-family: Montserrat;
      padding: 0 20px 0 50px;
      outline: none;
    }




    .login-block input:active,
    .login-block input:focus {
      border: 1px solid #ff656c;
    }

    .login-block button {
      width: 100%;
      height: 40px;
      background: #ff656c;
      box-sizing: border-box;
      border-radius: 5px;
      border: 1px solid #e15960;
      color: #fff;
      font-weight: bold;
      text-transform: uppercase;
      font-size: 14px;
      font-family: Montserrat;
      outline: none;
      cursor: pointer;
    }

    .login-block button:hover {
      background: #b71912;
    }
    /* enable absolute positioning */

    .inner-addon {
      position: relative;
    }
    /* style icon */

    .inner-addon .fa {
      position: absolute;
      padding: 10px;
      pointer-events: none;
    }
    /* align icon */

    .left-addon .fa {
      left: 0px;
    }

    .right-addon .fa {
      right: 0px;
    }
    /* add padding  */

    .left-addon input {
      padding-left: 30px;
    }

    .right-addon input {
      padding-right: 30px;
    }

    .rede {
      text-align: center;
    }

    .rede i {
      font-size: 30px;
      color: #545454;
    }
  </style>

  <!-- Bootstrap core CSS -->
  <link href="https://fonts.googleapis.com/css?family=Droid+Sans:400,700" rel="stylesheet">

  <link href="css/carrossel.css" rel="stylesheet">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M"
    crossorigin="anonymous">
  


</head>

<body style="padding:0px">

  <nav class="navbar navbar-expand-md navbar-light" style="background-color: #fff;padding-bottom:4%">
    <img src="/images/logo.png" alt="">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse"
      aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse justify-content-end" id="navbarCollapse">
      <ul class="navbar-nav">

        <li class="nav-item">
          <a class="nav-link" style="color:#b71912" href="/">A galeria</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/artista">Artistas</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/exposicao">Exposições</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/contacte">Contacte-nos</a>
        </li>
        @guest
        <li class="nav-item">
          <a class="nav-link" href="/login">Log in</a>
        </li>
        @endguest @if(Auth::user() && Auth::user()->utype_id == 3) @if(Auth::user()->requestArtist == 2)
        <button id="btnMostraModal" class="btn btn-primary btn-lg" style="display:none;" data-toggle="modal" data-target="#my">
          Launch modal
        </button>
        <div class="modal fade" id="my">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title">Desculpe</h5>
              </div>
              <div class="modal-body">
                <p>O seu pedido para vender obras no nosso site foi recusado, pode tambem entrar em contacto connosco atraves do separador
                <i>Contate-nos</i>
                </p>
              </div>
              <div class="modal-footer">
              <a href="/first" class="btn btn-primary">Seguinte</a>
              </div>
            </div>
          </div>
        </div>
        @endif
        <li class="nav-item">
          <a class="nav-link" href="/favoritos">Favoritos</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/compras"> Pedidos de compra</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
            Logout
          </a>
          <form class="nav-link" id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
          </form>
        </li>
        @endif @if(Auth::user() && Auth::user()->utype_id == 1)
        <script>
          document.location.href = '/home'
        </script>
        @endif @if(Auth::user() && Auth::user()->utype_id == 2) @if(Auth::user()->requestArtist == 2)
        <button id="btnMostraModal" class="btn btn-primary btn-lg" style="display:none;" data-toggle="modal" data-target="#my">
          Launch modal
        </button>
        <div class="modal fade" id="my">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title">Parabens</h5>
              </div>
              <div class="modal-body">
                <p>És um artista, agora poderas agendar Exposições e pedir para por obras a venda no site!</p>
              </div>
              <div class="modal-footer">
              <a href="/first" class="btn btn-primary">Quero entrar!</a>
              </div>
            </div>
          </div>
        </div>
        
        @endif @if(Auth::user()->requestArtist == 0)
        <li class="nav-item">
          <a class="nav-link" href="/home">
            Marcações
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/favoritos">Favoritos</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/compras"> Pedidos de compra</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
            Logout
          </a>
          <form class="nav-link" id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
          </form>
        </li>
        @endif @endif

      </ul>
    </div>
  </nav>
  <div>
    @yield('content')

  </div>

  <!-- FOOTER -->

  <footer class="container">
    <hr class="featurette-divider">
    <p class="float-right">
      <a href="/">Volte ao inicio</a>
    </p>
    <p>&copy; 2017 Carlos Teixeira&Nuno Gonçalves
      <div class="rede">
        <a href="https://www.facebook.com/quadrasoltas" target="_blank">
          <i class="fa fa-facebook-square" aria-hidden="true" style="padding-right: 2%"></i>
        </a>
        <a href="https://www.instagram.com/quadrasoltas2010" target="_blank">
          <i class="fa fa-instagram" aria-hidden="true" style="padding-right: 2%"></i>
        </a>
        <a href="https://www.pinterest.pt/quadrasoltas/" target="_blank">
          <i class="fa fa-pinterest" aria-hidden="true"></i>
        </a>
      </div>
    </p>

  </footer>

  </div>
  <!-- /.container -->


  <!-- Bootstrap core JavaScript
      ================================================== -->
  <!-- Placed at the end of the document so the pages load faster -->
  <script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n"
    crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb"
    crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn"
    crossorigin="anonymous"></script>
  <script type='text/javascript' src='http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js'></script>
  <script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n"
    crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb"
    crossorigin="anonymous"></script>
  <script src="../../dist/js/bootstrap.min.js"></script>
  <!-- Just to make our placeholder images work. Don't actually copy the next line! -->
  <script src="../../assets/js/vendor/holder.min.js"></script>
  <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
  <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
 
  <script>
    $(document).ready( function(){
      
      $('#btnMostraModal').click();
    });
    
  </script>
</body>

</html>