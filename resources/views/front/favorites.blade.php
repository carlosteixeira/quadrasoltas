@extends('layout.master') @section('title', 'Favoritos') @section('content')
<div style="width:90%; margin:2% auto">
    <h1>Favoritos</h1>
<hr>
    @if(!$favoritos->first())
    <div class="alert alert-info">
        Nao tem nenhum favorito a apresentar
    </div>
    @else
    <table style="margin-top:5%"class="table">
        <thead>
            <tr>
                <th>Nome da obra</th>
                <th>Nome do artista</th>
                <th>Exposição</th>
                <th>Preço</th>
            </tr>
        </thead>
        <tbody>
            @foreach($favoritos as $favorito)
            <tr>
                <td>{{$favorito->work->name}}</td>
                @if(!$favorito->work->user)
                    <td>Não especificado</td>
                @else
                    <td>{{$favorito->work->user->name}}</td>
                @endif
                @if(!$favorito->work->exposition)
                    <td>Não especificado</td>
                @else
                    <td>{{$favorito->work->exposition->name}}</td>
                @endif
                <td>{{$favorito->work->price}}€</td>
                <td>
                    <a href="/obras/{{$favorito->work->id}}/unfav">
                        <i style="color:red" class="fa fa-heart pull-right" aria-hidden="true"></i>
                    </a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    @endif
</div>
@endsection