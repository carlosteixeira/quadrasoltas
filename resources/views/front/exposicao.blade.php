@extends('layout.master')
@section('title', 'Exposições')

@section('content')


<div style="width:90%; margin:0 auto">

  
  @foreach($dates as $date)
    <h1>{{key($dates)}}</h1>
    <hr>
        <div class="row" >

      @foreach($date as $exposition)
      
              <div class="col-lg-4 col-sm-12 text-center mb-6">
                <a href="/exposicao/{{$exposition->id}}">
                <img src="{{$exposition->cover}}" style="width:100%;" alt="" >
                </a>
                <p></p>
              </div>
      @endforeach
    <?php
      next($dates);    
    ?>
              </div>
              @endforeach
              </div>
  



@endsection
