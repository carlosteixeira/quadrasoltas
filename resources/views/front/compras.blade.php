@extends('layout.master') @section('title', 'Pedidos de compras') @section('content')
<div style="width:90%; margin:2% auto">
    <h1>Pedidos de compras</h1>
<hr>
    @if(!$compras->first())
    <div class="alert alert-info">
        Nao tem nenhum pedido
    </div>
    @else
    <table style="margin-top:5%"class="table">
        <thead>
            <tr>
                <th>Nome da obra</th>
                <th>Nome do artista</th>
                <th>Exposição</th>
                <th>Preço</th>
                <th>Estado</th>
            </tr>
        </thead>
        <tbody>
            @foreach($compras as $compra)
            <tr>
                <td>{{$compra->work->name}}</td>
                @if(!$compra->work->user)
                    <td>Não especificado</td>
                @else
                    <td>{{$compra->work->user->name}}</td>
                @endif
                @if(!$compra->work->exposition)
                    <td>Não especificado</td>
                @else
                    <td>{{$compra->work->exposition->name}}</td>
                @endif
                <td>{{$compra->work->price}}€</td>
                <td>
                        @if($compra->was_accept == 0)
                            <span style="color:#ffca44"><b> Pendente </b></span>
                        @endif
                        @if($compra->was_accept == 1)
                        <span style="color:green"><b> Aceite </b></span>
                        @endif
                        @if($compra->was_accept == 2)
                        <span style="color:red"><b> Rejeitado </b></span> 
                        @endif
                    
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    @endif
</div>
@endsection