@extends('layout.master')
@section('title', 'Contacte-nos')

@section('content')
<div style="width:90%; margin:2% auto">
<h1>Contacte-nos</h1>
<hr>
@if ($message = Session::get('sucesso'))
        <div class="alert alert-success" role="alert" style="margin-top:1%">
             {{ $message }}
        </div>
    @endif
</div>

<div class="container" >

  <form action="/contacte" method="post">
  {{ csrf_field() }}
    <div class="form-group row">
      <label for="example-text-input" class="col-2 col-form-label">Name</label>
      <div class="col-10">
        <input name="name" class="form-control" type="text" value="" id="example-text-input">
      </div>
    </div>
    <div class="form-group row">
      <label for="example-text-input" class="col-2 col-form-label">Email</label>
      <div class="col-10">
        <input name="mail" class="form-control" type="email" value="" id="example-text-input">
      </div>
    </div>

    <div class="form-group">
      <textarea name="content" class="form-control" id="exampleTextarea" rows="7" placeholder="Gostaria de ..." required></textarea>
    </div>
    <button type="submit" class="btn btn-primary">Enviar</button>
  </form>

</div>
@endsection
