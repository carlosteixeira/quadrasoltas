@extends('layout.master') @section('title', 'Artistas')
@section('css')
<style>
    .fa-heart-o{
        color:gray
    }
    .fa-heart-o:hover{
        color:red
    }
    .fa-shopping-cart{
        color:gray
    }
    .fa-shopping-cart:hover{
        color:green
    }
</style>
@endsection
@section('content')
<div class="row">
    <div class="col-sm-4" style="margin-left:3%">
    <h1>Dados</h1>
        <hr>
        <div class="form-group">
            <label>Nome do Artista</label>
            <label class="form-control">{{$artista->name}}</label>
        </div>
        <div class="form-group">
            <label>Quantidade de obras</label>
            <label class="form-control">{{$artista->works->where('is_request', '=', 0)->count()}}</label>
        </div>
        <div class="form-group">
            @if($expos)
            <label>Exposições</label>
            @foreach($expos as $expo)
            <li>{{$expo->name}}</li>
            @endforeach @else
            <label>Não tem Exposições</label>
            @endif
        </div>
    </div>

    <div class="col-sm-1">
    </div>
    <div class="col-sm-6">
        <h1>Obras</h1>
        <hr>
        <div class="row">
            @foreach($works as $work)
            <div class="col-sm-6 col-md-6 ">
                <a href="/obras/{{$work->id}}">
                    <div class="abc" style="background-image:url('{{ $work->photo }}'); background-position: center;
                width:100%; height:200px;background-size: cover;background-repeat: no-repeat;">
                    </div>
                </a>
                <h4 style="text-align:center">{{ $work->name }}</h4>
                @guest
                <a href="/login/{{$work->id}}/pedido">
                    <i class="fa fa-shopping-cart pull-right" aria-hidden="true"></i>
                </a>
                <a href="/login/{{$work->id}}/favoritar">
                    <i class="fa fa-heart-o pull-right" aria-hidden="true"></i>
                </a>
            @endguest @if(Auth::user())
            @if(DB::select('select * from purchase where work_id = ? and user_id = ?', [$work->id, Auth::user()->id]))
                <i style="color:green" class="fa fa-shopping-cart pull-right" aria-hidden="true"></i>
            @else
                <a href="/obras/{{$work->id}}/pedido">
                    <i class="fa fa-shopping-cart pull-right" aria-hidden="true"></i>
                </a>
                @endif
                @if(DB::select('select * from favorites where work_id = ? and user_id = ?', [$work->id, Auth::user()->id]))
                    <a href="/obras/{{$work->id}}/unfav">
                        <i style="color:red" class="fa fa-heart pull-right" aria-hidden="true"></i>
                    </a>
                @else
                    <a href="/obras/{{$work->id}}/favoritar">
                        <i class="fa fa-heart-o pull-right" aria-hidden="true"></i>
                    </a>
                @endif
            @endif
            </div>
            @endforeach
        </div>
    </div>
</div> @endsection