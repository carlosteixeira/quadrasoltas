@extends('layout.master')
@section('title')
{{$work->name}}
@endsection
@section('content')
<div style="width:90%; margin:2% auto">
<div class="row">
    <div class="col-md-6">
        <img style="width:65%" src="{{$work->photo}}" alt="">
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label>Nome</label>
            <label class="form-control">{{ $work->name }}</label>
        </div>
        @if($work->user)
        <div class="form-group">
            <label>Autor</label>
            <label class="form-control">{{ $work->user->name }}</label>
        </div>
        @else
        <div class="form-group">
            <label>Autor</label>
            <label class="form-control">Autor não especificado</label>
        </div>
        @endif
        <div class="form-group">
            <label>Tipo</label>
            <label class="form-control">{{ $work->atype->name }}</label>
        </div>
        <div class="form-group">
            <label>Tecnica</label>
            <label class="form-control">{{ $work->technique }}</label>
        </div>
        <div class="form-group">
            <label>Preço</label>
            <label class="form-control">{{ $work->price }}€</label>
        </div>
        <div class="form-group">
            <label>Dimensão</label>
            <label class="form-control">{{ $work->dimension }}</label>
        </div>
        <div class="form-group">
            <label>Ano</label>
            <label class="form-control">{{ $work->workyear }}</label>
        </div>
    </div>
</div>
</div>

@endsection
