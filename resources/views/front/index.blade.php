@extends('layout.master')
@section('title', 'EspacoQ/QuadraSoltas')

@section('content')

                      <div class="container marketing">
                        <div class="row featurette">
                          <div class="col-md-7">
                            <h2 class="featurette-heading" style="margin-top: 2rem;">O nosso <span style="color:#b71912">espaço</span></h2>
                            <p class="lead" style="text-align:justify">O espaço de arte <i> Espaço <span style="color:#b71912">Q</span>/Quadra<span style="color:#b71912">S</span>oltas</i> é uma associação cultural sem fins lucrativos que conta com um largo números de membros
              , um grupo de professores e artistas que tinham como objetivo criar um espaço onde pudessem dar reconhecimento à arte, assim como
              a todos os artistas que por lá passassem. A abertura do espaço aconteceu em novembro de 2010 na Rua Miguel Bombarda na cidade do
              Porto e agora tem como missão evoluir cada vez mais e celebrar a arte e a cultura sob todas as formas.</p>
                          </div>
                          <div class="col-md-5">
                            <img class="featurette-image img-fluid mx-auto" style="margin-top: 2rem;" src="images/front.jpg" alt="Galeria">
                          </div>
                        </div>

                        <hr class="featurette-divider">
                        <div class="row featurette">
                          <div class="col-md-12">
                            <h2 class="featurette-heading" style="margin-top: 1rem; margin-bottom: 2rem; text-align:center">Encontre-nos!</h2>
                            <div id="map" style="width:100%;height:300px;"></div>
                              <script >
                         			    function initMap() {
                                    var contentString ='<p><i class="fa fa-map-marker" style="padding-right:5%; font-size:16px"></i>Rua Miguel Bombarda 529, Porto | 4050-379</p><p><i class="fa fa-phone" style="padding-right:5%; font-size:16px"></i>+365 226 001 007</p><p><i class="fa fa-envelope" style="padding-right:5%; font-size:16px"></i>quadrasoltas@gmail.com</p>';
                                    var myLatLng = {lat: 41.151301, lng: -8.621210};
                         		           var galeria = {lat: 41.149695, lng: -8.620372};
                                       var infowindow = new google.maps.InfoWindow({
                                           content: contentString
                                         });
                                       var mapOptions = {
                                           center: myLatLng,
                                           zoom: 15,
                                           disableDefaultUI: true,
                                           mapTypeId: google.maps.MapTypeId.MAPS
                                       }
                                       var map = new google.maps.Map(document.getElementById("map"), mapOptions);


                                       marker = new google.maps.Marker({
                                          map: map,
                                          draggable: false,
                                          animation: google.maps.Animation.BOUNCE,
                                          position: galeria,
                                          title: 'Espaço Q | QuadraSoltas'
                                        });
                                        infowindow.open(map,marker);

                                      }

                                      function toggleBounce() {
                                          marker.setAnimation(google.maps.Animation.BOUNCE);
                         		      }
                         		    </script>

                         		    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBBFMtDLv-ijHr3AaEcje9ev3mypGrORcE&callback=initMap" async defer></script>
                          </div>
                        </div>



                        <!-- /END THE FEATURETTES -->

@endsection
