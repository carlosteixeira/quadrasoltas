@extends('layout.master')
@section('title')
{{$exposition->name}}
@endsection
@section('css')
<style>
    .fa-heart-o{
        color:gray
    }
    .fa-heart-o:hover{
        color:red
    }
    .fa-shopping-cart{
        color:gray
    }
    .fa-shopping-cart:hover{
        color:green
    }
</style>
@endsection
@section('content')
<div style="width:90%; margin:2% auto">
    <h1>{{$exposition->name}}</h1>
    <hr>

    <div class="row">
    @foreach($works as $work)
    <a href="/obras/{{$work->id}}">
        <div class="col-lg-4 col-sm-12 text-center mb-6">
            <div class="rounded" style="background-image:url('{{ $work->photo }}'); background-position: center;
            width:100%; height:200px;background-size: cover;background-repeat: no-repeat;" >     
            </div>
        </a>
            <h5 style="padding:2%">
                {{$work->name}}
                @guest
                <a href="/login/{{$work->id}}/pedido">
                    <i class="fa fa-shopping-cart pull-right" aria-hidden="true"></i>
                </a>
                <a href="/login/{{$work->id}}/favoritar">
                    <i class="fa fa-heart-o pull-right" aria-hidden="true"></i>
                </a>
            @endguest @if(Auth::user())
            @if(DB::select('select * from purchase where work_id = ? and user_id = ?', [$work->id, Auth::user()->id]))
                <i style="color:green" class="fa fa-shopping-cart pull-right" aria-hidden="true"></i>
            @else
                <a href="/obras/{{$work->id}}/pedido">
                    <i class="fa fa-shopping-cart pull-right" aria-hidden="true"></i>
                </a>
                @endif
                @if(DB::select('select * from favorites where work_id = ? and user_id = ?', [$work->id, Auth::user()->id]))
                    <a href="/obras/{{$work->id}}/unfav">
                        <i style="color:red" class="fa fa-heart pull-right" aria-hidden="true"></i>
                    </a>
                @else
                    <a href="/obras/{{$work->id}}/favoritar">
                        <i class="fa fa-heart-o pull-right" aria-hidden="true"></i>
                    </a>
                @endif
            @endif
            </h5>
        </div>
    @endforeach
    </div>       
</div>
@endsection
