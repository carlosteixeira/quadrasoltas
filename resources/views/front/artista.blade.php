@extends('layout.master') @section('title', 'Artistas') @section('content')
<div style="margin-top:20px" class="container">
@if ($message = Session::get('sucess'))
        <div class="alert alert-info" role="alert" style="margin-top:1%">
             {{ $message }}
        </div>
    @endif
@if(Auth::user())
@if(Auth::user()->utype_id ==3)
  <p class="pull-right">
    <span style="text-transform: capitalize;">{{Auth::user()->name}}</span> gostaria de vender obras?
    <a href="/expor">Click aqui</a>
  </p>
  @endif
  @else
  <p class="pull-right">Gostaria de vender obras?
    <a href="/login/expor">Click aqui</a>
  </p>
  @endif 
<h1>Artistas das QuadraSoltas</h1>
<hr>
  @if(!$artistas->first())
  <div class="alert alert-info">
    Nao tem nenhum artista a apresentar
  </div>
  @else
  <table class="table">
    <tr>
      <th>Nomedo artista</th>
      <th>Quantidade de Obras</th>
    </tr>
    @foreach($artistas as $artista)
    <tr>
      <td>{{$artista->name}}</td>
      
      @if($artista->works->where('is_request', '=', 0)->count() == 0)
        <td>Não tem nenhuma obra</td>
      @else
        <td>{{$artista->works->where('is_request', '=', 0)->count()}}</td>
      @endif
      <td>
        <a href="/artista/{{$artista->id}}">Ver mais</a>
      </td>
    </tr>
    @endforeach
  </table>
  @endif
</div>

@endsection