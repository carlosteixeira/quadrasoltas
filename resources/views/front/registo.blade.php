@extends('layout.master')
@section('title', 'Registo')

@section('content')
<div class="container" style="margin-top:10%">

  <form action="{{ route('register') }}" method="post">
     {{ csrf_field() }}
      <div class="login-block">
        <div class="inner-addon left-addon">
            <i class="fa fa-user" aria-hidden="true"></i>
            <input type="text" class="form-control" placeholder="Nome" value="{{ old ('name') }}" name="nome" required/>
            @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
        </div>
        <div class="inner-addon left-addon">
            <i class="fa fa-at" aria-hidden="true"></i>
            <input type="email" class="form-control" value="{{ old ('email') }}" placeholder="Email" name="email" required/>
            @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
        </div>
          <div class="inner-addon left-addon">
              <i class="fa fa-lock" aria-hidden="true"></i>
              <input type="password" value="{{ old ('address') }}" placeholder="Password" name="password" required/>
              @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
          </div>
          <div class="inner-addon left-addon">
              <i class="fa fa-lock" aria-hidden="true"></i>
              <input type="password" value="" placeholder="Introduza novamente a password" name="password_confirmation" required/>
          </div>
          <div class="inner-addon left-addon">
            <i class="fa fa-phone" aria-hidden="true"></i>
            <input type="text" value="{{ old ('phone') }}" placeholder="Telemovel" name="phone" required/>
          </div>
          <div class="inner-addon left-addon">
            <i class="fa fa-home" aria-hidden="true"></i>
            <input type="text" name="address" placeholder="Morada" value="{{ old ('address') }}" name="address" required/>
            @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
          </div>
          <button type="submit" class="btn btn-danger">Registar</button>
    </form>
</div>
@endsection


<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Register</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="">
                        

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Register
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>