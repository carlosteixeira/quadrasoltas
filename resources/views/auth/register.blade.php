@extends('layout.master')
@section('title', 'Registo')
@section('content')

<div class="container" style="margin-top:10%">

  <form action="{{ route('register') }}" method="post">
     {{ csrf_field() }}
      <div class="login-block">
        <div class="inner-addon left-addon">
            <i class="fa fa-user" aria-hidden="true"></i>
            <input type="text" class="form-control" placeholder="Nome" value="{{ old ('name') }}" name="name" required/>
            @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
        </div>
        <div class="inner-addon left-addon">
            <i class="fa fa-at" aria-hidden="true"></i>
            <input type="email" class="form-control" value="{{ old ('email') }}" placeholder="Email" name="email" required/>
            @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
        </div>
          <div class="inner-addon left-addon">
              <i class="fa fa-lock" aria-hidden="true"></i>
              <input type="password"  placeholder="Password" name="password" required/>
              @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
          </div>
          <div class="inner-addon left-addon">
              <i class="fa fa-lock" aria-hidden="true"></i>
              <input type="password" value="" placeholder="Introduza novamente a password" name="password_confirmation" required/>
          </div>
          <div class="inner-addon left-addon">
            <i class="fa fa-phone" aria-hidden="true"></i>
            <input type="text" value="{{ old ('phone') }}" placeholder="Telemovel" name="phone" required/>
            @if ($errors->has('phone'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
          </div>
          <div class="inner-addon left-addon">
            <i class="fa fa-home" aria-hidden="true"></i>
            <input type="text" name="address" placeholder="Morada" value="{{ old ('address') }}" name="address" required/>
            @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
          </div>
          <button type="submit" class="btn btn-danger">Registar</button>
    </form>
</div>

@endsection
