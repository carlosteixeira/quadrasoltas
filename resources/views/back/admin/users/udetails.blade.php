@extends('layout.app')
@section('title','User')
@section('page_heading')
Perfil de {{$user->name}}
@endsection
@section('content')



        <div class="form-group">
            <label for="exampleInputEmail1">Email</label>
            <label class="form-control">{{ $user->email }}</label>
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">Morada</label>
            <label class="form-control">{{ $user->address }}</label>
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">Telefone</label>
            <label class="form-control">{{ $user->phone }}</label>
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">Tipo</label>
            <label class="form-control">{{ $user->utype->name }}</label>
        </div>
        <form action="/user/{{$user->id}}" method="post">
            {{ csrf_field() }}
            {{ method_field('DELETE') }}
            <input type="submit" class="btn btn-danger pull-right" value="Apagar">
        </form>




    
@endsection