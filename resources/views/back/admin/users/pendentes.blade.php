@extends('layout.app')
@section('title','Users')
@section('page_heading','Promover a Artistas')
@section('content')
@if ($message = Session::get('sucess'))
        <div class="alert alert-success" role="alert" style="margin-top:1%">
             {{ $message }}
        </div>
    @endif
    @if ($message = Session::get('danger'))
        <div class="alert alert-danger" role="alert" style="margin-top:1%">
             {{ $message }}
        </div>
    @endif
@if(!$users->first())
  <div class="alert alert-info">
    Nao tem nenhum pedido a apresentar
  </div>
  @else
<table class="table">
  <thead>
    <tr>
      <th>Nome</th>
      <th>E-Mail</th>
      <th>Telefone</th>


    </tr>
  </thead>
  <tbody>


<tbody>
  @foreach($users as $user)
    <tr>
      <td>{{$user->name}}</td>
      <td>{{$user->email}}</td>
      <td>{{$user->phone}}</td>
      <td><a  class="btn btn-success" href="/user/aceitar/{{$user->id}}">Aceitar</a>
      <a  class="btn btn-danger" href="/user/rejeitar/{{$user->id}}">Rejeitar</a>
      </td>
    </tr>
    @endforeach
  </tbody>
</table>
@endif
@endsection