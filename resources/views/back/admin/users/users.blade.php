@extends('layout.app')
@section('title','Users')
@section('page_heading','Todos os utilizadores')
@section('content')

<table class="table">
  <thead>
    <tr>
      <th>Nome</th>
      <th>E-Mail</th>
      <th>Telefone</th>


    </tr>
  </thead>
  <tbody>


<tbody>
  @foreach($users as $user)
    <tr>
      <td>{{$user->name}}</td>
      <td>{{$user->email}}</td>
      <td>{{$user->phone}}</td>
      <td><a  class="btn btn-primary" href="/user/{{$user->id}}">Ver</a></td>
    </tr>
    @endforeach
  </tbody>
</table>
@endsection