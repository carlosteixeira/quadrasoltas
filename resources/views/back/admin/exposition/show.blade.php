@extends('layout.app')
@section('title')
{{$exposition->name}}
@endsection
@section('page_heading')
{{$exposition->name}}
@endsection
@section('content')
<div style="width:90%; margin:2% auto">
@if ($message = Session::get('sucesso'))
<div class="alert alert-success" role="alert" style="margin-top:1%">
     {{ $message }}
</div>
@endif
    <div class="row">
        <div class="col-md-6">
            <img src="{{$exposition->cover}}" style="width:100%" alt="">
        </div>
        <div class="col-md-6">
        <div class="form-group">
        <label>Nome da exposição</label>
        <label class="form-control">{{ $exposition -> name }}</label>
    </div>
    <div class="form-group">
        <label>Data inicial</label>
        <label class="form-control">{{ DateTime::createFromFormat('Y-m-d', $exposition -> inicialdate)->format('d-m-Y')}}</label>
    </div>
    <div class="form-group">
        <label>Data Final</label>
        <label class="form-control">{{ DateTime::createFromFormat('Y-m-d', $exposition -> finaledate)->format('d-m-Y')}}</label>
    </div>
    <div class="form-group">
        <label>Quantidade de obras</label>
        <label class="form-control">{{$qtn}}
    </div>
    <div class="form-group">
        @if($abc)   
        <label>Artistas</label> 
            @foreach($abc as $artist)
                <li>{{$artist}}</li>
            @endforeach
        @else
            <label>Não tem Artistas</label>
        @endif
    </div>
            <div class="row" >
                <a class="btn btn-success pull-right" href="/exposicao/{{$exposition->id}}/edit">Alterar</a>
            </div>
        </div>
    </div>
</div>




@endsection
