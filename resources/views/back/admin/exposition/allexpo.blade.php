@extends('layout.app') @section('title','Exposição') @section('page_heading','Todas as Exposições') @section('content') 

@if ($message = Session::get('sucesso'))
<div class="alert alert-success" role="alert" style="margin-top:1%">
     {{ $message }}
</div>
@endif
@if(!$dates)    
    <div class="alert alert-info" >
        Nao tem nenhuma marcação a apresentar
    </div>
    @else
@foreach($dates as $date)
<div style="width:90%; margin:0 auto">
  <h3>{{key($dates)}}</h3>
  <hr>
  <div class="row">
    @foreach($date as $exposition)
    <div class="col-lg-4 col-sm-12 text-center mb-6">
      <a href="/exposicao/{{$exposition->id}}">
        <img src="{{$exposition->cover}}" style="width:100%;" alt="">
      </a>
      <p></p>
    </div>
    @endforeach
    <?php
      next($dates);    
    ?>
  </div>
</div>
 @endforeach @endif @endsection