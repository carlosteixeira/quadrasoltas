@extends('layout.app') @section('title') Pedido {{$marking->id}} @endsection @section('page_heading') Pedido feito por {{$marking->user->name}}
@endsection @section('content')
<div style="width:90%; margin:2% auto">
    <div class="form-group">
        <label>Nome da exposição</label>
        <label class="form-control">{{ $marking->name }}</label>
    </div>
    <div class="form-group">
        <label>Data inicial</label>
        <label class="form-control">{{ $marking->datewantedinicial }}</label>
    </div>
    <div class="form-group">
        <label>Data Final</label>
        <label class="form-control">{{ $marking->datewantedfinal }}</label>
    </div>
    <div class="row">
        <form action="/exposicao/{{$marking->id}}" method="post">
            {{ csrf_field() }} 
            {{ method_field('DELETE') }}
            <input type="submit" class="btn btn-danger pull-right" value="Apagar">
        </form>
        <a class="btn btn-success pull-left"  href="/exposicao/aceitar/{{$marking->id}}">Aceitar</a>
    </div>
</div>
@endsection