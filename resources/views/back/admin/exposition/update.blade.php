@extends('layout.app')
@section('title')
{{$exposition->name}}
@endsection
@section('page_heading')
Editar {{$exposition->name}}
@endsection
@section('content')
<div style="width:90%; margin:2% auto">
    <div class="row">
        <form action="/exposicao/{{ $exposition->id }}" method="post">
            {{ csrf_field() }} 
            {{ method_field('PUT') }}
            <div class="col-md-6">
                <img src="{{$exposition->cover}}" style="width:100%" alt="">
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Nome da exposição</label>
                    <input class="form-control" type="text" name="name" value="{{$exposition -> name}}">
                </div>
                <div class="form-group">
                    <label>Data inicial</label>
                    <input class="form-control" type="date" name="inicialdate" value="{{$exposition -> inicialdate}}">
                </div>
                <div class="form-group">
                    <label>Data Final</label>
                    <input class="form-control" type="date" name="finaledate" value="{{$exposition -> finaledate}}">
                </div>
                <div class="form-group">
                    <label>Quantidade de obras</label>
                    <label style="background-color: gainsboro;" class="form-control">{{$qtn}}</label>
                </div>
                <div class="form-group">
                    @if($abc)
                    <label>Artistas</label>
                    @foreach($abc as $artist)
                    <li>{{$artist}}</li>
                    @endforeach @else
                    <label>Não tem Artistas</label>
                    @endif
                </div>
                <div class="row">
                    <input class="btn btn-primary pull-right" type="submit" value="Editar">
                </div>
            </div>
        </form>
    </div>
</div>
@endsection