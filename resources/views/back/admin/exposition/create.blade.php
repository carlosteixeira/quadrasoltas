@extends('layout.app')
@section('title','Exposição')
@section('page_heading','Criar uma Exposição')
@section('content')


<form action="/exposicao" method="post" enctype="multipart/form-data">
{{ csrf_field() }}
    <div class="form-group">
        <label >Nome da exposição</label>
        <input type="text" class="form-control" name="name" placeholder="Coloque o nome" value="{{ old('name') }}">
        @if ($errors->has('name'))
            <span class="help-block">
                <strong>Falta nome da exposição</strong>
            </span>
        @endif
    </div>
    <div class="row">
        <div class="col-md-6">
            <label >Data de inicio</label>
            <input type='date'  class="form-control" name="inicialdate" value="{{ old('inicialdate') }}"/>
            @if ($errors->has('inicialdate'))
                <span class="help-block">
                    <strong>Insira uma data da início</strong>
                </span>
            @endif
        </div>
        <div class="col-md-6">
            <label >Data de fim</label>
            <input type='date'  class="form-control" name="finaledate"  @if (!$errors->has('finaledate')) value="{{ old('finaledate') }}"@endif/>
            @if ($errors->has('finaledate'))
                <span class="help-block">
                    <strong>Insira uma data da termino</strong>
                </span>
            @endif
        </div>
    </div>
  <div class="form-group">
    <label>Capa</label>
    <input type="file" class="form-control-file" name="cover" aria-describedby="fileHelp">
    @if ($errors->any())
            <span class="help-block">
                <strong>Insira a foto novamente</strong>
            </span>
        @endif
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection