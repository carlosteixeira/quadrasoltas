@extends('layout.app') @section('title','Marcações') @section('page_heading','Pedidos de marcação') @section('content')
@if ($message = Session::get('sucesso'))
<div class="alert alert-success" role="alert" style="margin-top:1%">
     {{ $message }}
</div>
@endif
@if(!$markings->first())    
    <div class="alert alert-info" >
        Nao tem nenhuma marcação a apresentar
    </div>
    @else
<table class="table">
    <thead>
      <tr>
        <th>Nome da obra</th>
        <th>Artista</th>
      </tr>
    </thead>
    <tbody>
    @foreach($markings as $marking)
      <tr>
        <td>{{$marking->name}}</td>
        <td>{{$marking->user->name}}</td>
        <td><a class="btn btn-primary" href="/exposicao/pedido/{{$marking->id}}">Ver</a></td>
      </tr>
      @endforeach
    </tbody>
  </table>
  @endif
@endsection