@extends('layout.app')
@section('title','Mensagens')
@section('page_heading','Todas as mensagens')
@section('content')
@if ($message = Session::get('sucesso'))
<div class="alert alert-success" role="alert" style="margin-top:1%">
     {{ $message }}
</div>
@endif
    @if(!$contacts->first())    
    <div class="alert alert-info" >
        Nao tem nenhuma mensagem a apresentar
    </div>
    @else
<table class="table">
  <thead>
    <tr>
      <th>Nome</th>
      <th>E-Mail</th>
      <th>Data</th>
      <th></th>
      <th>Status</th>

    </tr>
  </thead>
  <tbody>

 
  @foreach($contacts as $contact)
    <tr>
      <td>{{$contact->name}}</td>
      <td>{{$contact->mail}}</td>
      
      <td>{{ DateTime::createFromFormat('Y-m-d H:i:s', $contact->created_at)->format('d-m-Y')}} às {{ DateTime::createFromFormat('Y-m-d H:i:s', $contact->created_at)->format('H:i')}}h</td>
      <td><a  class="btn btn-primary" href="/contacte/{{$contact->id}}">Ver</a></td>
      <td>
        @if( $contact->is_read == 0)
            <span class="btn btn-danger"><i class="fa fa-times" aria-hidden="true"></i></span>
        @else
            <span class="btn btn-success">Lido</span>
        @endif
      </td>
    </tr>
    @endforeach
  </tbody>
</table>
@endif
@endsection