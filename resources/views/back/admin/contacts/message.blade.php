@extends('layout.app')
@section('title','Mensagens')
@section('page_heading')
Mensagem de {{$message->name}}
@endsection
@section('content')



        <div class="form-group">
            <label for="exampleInputEmail1">Email</label>
            <label class="form-control">{{ $message->mail }}</label>
        </div>
        
            <label class="form-control"  style="padding-bottom:10%"> {{ $message->content }} </label>
        </div>
        <a class="pull-left" href="{{ URL::previous() }}">Go Back</a>
        <form action="/contacte/{{$message->id}}" method="post">
            {{ csrf_field() }}
            {{ method_field('DELETE') }}
            <input type="submit" class="btn btn-danger pull-right" value="Apagar">
        </form>




    
@endsection