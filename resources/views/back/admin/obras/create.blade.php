@extends('layout.app')
@section('title','Obra')
@section('page_heading','Criar uma Obra')
@section('content')



<form action="/obras" method="post" enctype="multipart/form-data">
{{ csrf_field() }}
    <div class="form-group">
        <label >Nome da obra</label>
        <input type="text" class="form-control" name="name" placeholder="Coloque o nome">
        @if ($errors->has('name'))
            <span class="help-block">
                <strong>Falta nome da obra</strong>
            </span>
        @endif
    </div>
    <div class="row">
        <div class="col-md-6 form-group">
            <label for="content">Tipo de Produto:</label>
            <select class="form-control" name="atype">
            @foreach($types as $type)
            <option value="{{$type->id}}">{{$type->name}}</option>
            @endforeach
            </select>
            @if ($errors->has('atype'))
            <span class="help-block">
                <strong>Falta tipo de arte</strong>
            </span>
        @endif
        </div>
            <div class="col-md-6 form-group">
            <label for="content">Artista</label>
            <select class="form-control" name="user">
            <option value="">Nao especificar Artista</option>
            @foreach($artists as $artist)
            <option value="{{$artist->id}}">{{$artist->name}}</option>
            @endforeach
            </select>
            @if ($errors->has('user'))
            <span class="help-block">
                <strong>Falta o Artista</strong>
            </span>
        @endif
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 form-group">
            <label >Técnica</label>
            <input type="text" class="form-control" name="technique" placeholder="Técnica">
            @if ($errors->has('technique'))
            <span class="help-block">
                <strong>Falta uma tecnica</strong>
            </span>
        @endif
        </div>
        <div class="col-md-6 form-group">
            <label >Preço</label>
            <input type="text" class="form-control" name="price" placeholder="Preço">
            @if ($errors->has('price'))
            <span class="help-block">
                <strong>Falta um preço</strong>
            </span>
        @endif
        </div>
    </div>     
    <div class="row">
        <div class="col-md-6 form-group">
            <label >Ano</label>
            <input type="text" class="form-control" name="workyear" placeholder="Ano">
            @if ($errors->has('workyear'))
            <span class="help-block">
                <strong>Falta o ano</strong>
            </span>
        @endif
        </div>
        <div class="col-md-6 form-group">
            <label >Dimensões</label>
            <input type="text" class="form-control" name="dimension" placeholder="">
            @if ($errors->has('dimension'))
            <span class="help-block">
                <strong>Falta as dimensões</strong>
            </span>
        @endif
        </div>
    </div>
    <div class="form-group">
        <label for="content">Exposição</label>
        <select class="form-control" name="exposition">
        <option value="">Nao especificar Exposição</option>
        @foreach($expos as $expo)
        <option value="{{$expo->id}}">{{$expo->name}}</option>
        @endforeach
        </select>
        @if ($errors->has('exposition'))
            <span class="help-block">
                <strong>Falta especificar se pertence a alguma exposição</strong>
            </span>
        @endif
    </div>
    <div class="form-group">
    <label>Foto da obra</label>
    <input type="file" class="form-control-file" name="photo" aria-describedby="fileHelp">
    @if ($errors->has('photo'))
            <span class="help-block">
                <strong>Insira uma foto valida</strong>
            </span>
        @endif
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection


<!-- <div class="form-group">
        <label >Nome da obra</label>
        <input type="text" class="form-control" name="name" placeholder="Coloque o nome">
        @if ($errors->has('name'))
            <span class="help-block">
                <strong>Falta nome da exposição</strong>
            </span>
        @endif
    </div>
    <div class="form-group">
        <label >Nome da obra</label>
        <input type="text" class="form-control" name="name" placeholder="Coloque o nome">
        @if ($errors->has('name'))
            <span class="help-block">
                <strong>Falta nome da exposição</strong>
            </span>
        @endif
    </div>
    <div class="form-group">
        <label >Nome da obra</label>
        <input type="text" class="form-control" name="name" placeholder="Coloque o nome">
        @if ($errors->has('name'))
            <span class="help-block">
                <strong>Falta nome da exposição</strong>
            </span>
        @endif
    </div>
  <div class="form-group">
    <label>Capa</label>
    <input type="file" class="form-control-file" name="cover" aria-describedby="fileHelp">
    @if ($errors->has('cover'))
            <span class="help-block">
                <strong>{{$errors}}</strong>
            </span>
        @endif
  </div> -->