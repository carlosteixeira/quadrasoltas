@extends('layout.app') @section('title','Obras') @section('page_heading','Todas as Obras') @section('content')
<div class="row">

    <div class="col-sm-8" style="margin-left:3%">
    @if ($message = Session::get('sucesso'))
<div class="alert alert-success" role="alert" style="margin-top:1%">
     {{ $message }}
</div>
@endif
        <div class="row">
            @foreach($works as $work)
            <div class="col-sm-6 col-md-4 ">
                <a href="/obras/{{$work->id}}">
                    <div class="abc" style="background-image:url('{{ $work->photo }}'); background-position: center;
                width:100%; height:200px;background-size: cover;background-repeat: no-repeat;">
                    </div>
                </a>
                <h4 style="text-align:center">{{ $work->name }}</h4>
            </div>
            @endforeach
        </div>
        {{ $works->links() }}
    </div>
    <div class="panel-group col-sm-3 ">
        <div class="panel panel-default">
            <div class="panel-heading">
                    <h4 class="panel-title">
                        Filtros
                    </h4>
            </div>
                <div class="panel-body">
                    <form method="post" action="/obras/search">
                    {{ csrf_field() }}
                        <div class="form-group">
                            <label for="exampleInputEmail1">Nome da obra</label>
                            <input type="text" class="form-control" name="name" >
                        </div>
                        <button type="submit" class="btn btn-default">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection