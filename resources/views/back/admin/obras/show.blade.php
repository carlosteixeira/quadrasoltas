@extends('layout.app')
@section('title')
{{$work->name}}
@endsection
@section('page_heading') 
{{$work->name}}
@endsection
@section('content')
<div style="width:90%; margin:2% auto">
<div class="row">
    <div class="col-md-6">
        <img style="width:70%" src="{{$work->photo}}" alt="">
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label>Nome</label>
            <label class="form-control">{{ $work->name }}</label>
        </div>
        @if($work->user)
        <div class="form-group">
            <label>Autor</label>
            <label class="form-control">{{ $work->user->name }}</label>
        </div>
        @else
        <div class="form-group">
            <label>Autor</label>
            <label class="form-control">Autor não especificado</label>
        </div>
        @endif
        @if($work->exposition)
        <div class="form-group">
            <label>Exposição</label>
            <label class="form-control">{{ $work->exposition->name }}</label>
        </div>
        @else
        <div class="form-group">
            <label>Exposição</label>
            <label class="form-control">Não está em nenhuma exposição</label>
        </div>
        @endif
        <div class="form-group">
            <label>Tipo</label>
            <label class="form-control">{{ $work->atype->name }}</label>
        </div>
        <div class="form-group">
            <label>Tecnica</label>
            <label class="form-control">{{ $work->technique }}</label>
        </div>
        <div class="form-group">
            <label>Preço</label>
            <label class="form-control">{{ $work->price }}</label>
        </div>
        <div class="form-group">
            <label>Dimensão</label>
            <label class="form-control">{{ $work->dimension }}</label>
        </div>
        <div class="form-group">
            <label>Ano</label>
            <label class="form-control">{{ $work->workyear }}</label>
        </div>
    </div>
</div>
@endsection

