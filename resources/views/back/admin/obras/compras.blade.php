@extends('layout.app')
@section('title','Pedidos de compras')
@section('page_heading','Pedidos de compras')
@section('content')
@if ($message = Session::get('sucess'))
        <div class="alert alert-success" role="alert" style="margin-top:1%">
             {{ $message }}
        </div>
    @endif
    @if ($message = Session::get('danger'))
        <div class="alert alert-danger" role="alert" style="margin-top:1%">
             {{ $message }}
        </div>
    @endif
@if(!$compras->first())
  <div class="alert alert-info">
    Nao tem nenhum pedido a apresentar
  </div>
  @else
<table class="table">
  <thead>
    <tr>
      <th>Nome da obra</th>
      <th>E-Mail</th>
      <th>Telefone</th>


    </tr>
  </thead>
  <tbody>


<tbody>
  @foreach($compras as $compra)
    <tr>
      <td>{{$compra->work->name}}</td>
      <td>{{$compra->user->email}}</td>
      <td>{{$compra->user->phone}}</td>
      <td><a class="btn btn-success" href="/compra/aceitar/{{$compra->id}}">Aceitar</a>
      <a class="btn btn-danger" href="/compra/rejeitar/{{$compra->id}}">Rejeitar</a>
      </td>
    </tr>
    @endforeach
  </tbody>
</table>
@endif
@endsection