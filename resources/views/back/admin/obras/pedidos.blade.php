@extends('layout.app') @section('title','Obras') @section('page_heading','Pedidos') @section('content')

@if(!$works->first())    
    <div class="alert alert-info" >
        Nao tem nenhuma obra a apresentar
    </div>
    @else
<table class="table">
    <thead>
      <tr>
        <th>Nome da obra</th>
        <th>Artista</th>
        <th>Price</th>
      </tr>
    </thead>
    <tbody>
    @foreach($works as $work)
      <tr>
        <td>{{$work->name}}</td>
        @if(!$work->user)
          <td>Não especificado</td>
        @else
          <td>{{$work->user->name}}</td>
        @endif
       
        <td>{{$work->price}}</td>
        <td><a class="btn btn-primary" href="/obras/pedido/{{$work->id}}">Ver</a></td>
      </tr>
      @endforeach
    </tbody>
  </table>
  @endif
@endsection