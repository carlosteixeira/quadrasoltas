@extends('layout.artist') @section('title','Obras') @section('content')

<div style="width:90%;margin:2% auto">
    <h2>As minhas exposições</h2>
    <hr>
    @if ($message = Session::get('sucesso'))
    <div class="alert alert-success" role="alert" style="margin-top:1%">
        {{ $message }}
    </div>
    @endif
    <div class="row">
        @foreach($expos as $expo)
        <div class="col-lg-4 col-sm-12 text-center mb-6">
            <a href="/exposicao/{{$expo->id}}">
                <img src="{{$expo->cover}}" style="width:100%;" alt="">
            </a>
            <p></p>
        </div>
        @endforeach
        <div style="text-align:center" class="col-sm-6 col-md-2 ">
            <a href="/exposicao/request">
                <i style="font-size:180px; color:gray; " class="fa fa-plus-square-o" aria-hidden="true"></i>
            </a>
            <h4 style="text-align:center">Agendamento de uma exposição</h4>
        </div>
    </div>
</div>
@endsection