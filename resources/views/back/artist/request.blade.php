@extends('layout.artist') @section('title','Obra') @section('content')
<div style="width:90%;margin: 3% auto">
    <h2>Pedir para adicionar</h2>
    <hr>
</div>
<div class="container">
    <form action="/obras/storeRequest" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="form-group">
            <label>Nome da obra</label>
            <input type="text" class="form-control" name="name" > @if ($errors->has('name'))
            <span class="help-block">
                <strong>Insira um nome válido</strong>
            </span>
            @endif
        </div>
        <div class="row">
            <div class="col-md-6 form-group">
                <label for="content">Tipo:</label>
                <select class="form-control" name="atype">
                    @foreach($types as $type)
                    <option value="{{$type->id}}">{{$type->name}}</option>
                    @endforeach
                </select>
                @if ($errors->has('atype_id'))
                <span class="help-block">
                    <strong>Tem de especificar uma arte</strong>
                </span>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 form-group">
                <label>Técnica</label>
                <input type="text" class="form-control" name="technique" > @if ($errors->has('technique'))
                <span class="help-block">
                    <strong>Insira uma tecnica</strong>
                </span>
                @endif
            </div>
            <div class="col-md-6 form-group">
                <label>Preço</label>
                <input type="text" class="form-control" name="price" > @if ($errors->has('price'))
                <span class="help-block">
                    <strong>Insira um preço</strong>
                </span>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 form-group">
                <label>Ano</label>
                <input type="text" class="form-control" name="workyear" > @if ($errors->has('workyear'))
                <span class="help-block">
                    <strong>Insira um ano</strong>
                </span>
                @endif
            </div>
            <div class="col-md-6 form-group">
                <label>Dimensões</label>
                <input type="text" class="form-control" name="dimension" > @if ($errors->has('dimension'))
                <span class="help-block">
                    <strong>Insira as dimensões</strong>
                </span>
                @endif
            </div>
        </div>
        <div class="form-group">
            <label>Imagem</label>
            <input type="file" class="form-control-file" name="photo" aria-describedby="fileHelp"> @if ($errors->has('photo'))
            <span class="help-block">
                <strong>Insira uma imagem</strong>
            </span>
            @endif

        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>
@endsection