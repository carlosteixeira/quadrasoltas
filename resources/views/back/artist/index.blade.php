@extends('layout.artist') @section('title','Obras') @section('content')

<div style="width:90%;margin:2% auto">
<h2>As minhas obras</h2>
<hr>
@if ($message = Session::get('sucesso'))
    <div class="alert alert-success" role="alert" style="margin-top:1%">
        {{ $message }}
    </div>
    @endif
        <div class="row">
            @foreach($works as $work)
            <div class="col-sm-6 col-md-2 ">
                <a href="/obras/{{$work->id}}">
                    <div class="abc" style="background-image:url('{{ $work->photo }}'); background-position: center;
                width:100%; height:200px;background-size: cover;background-repeat: no-repeat;">
                    </div>
                </a>
                <h4 style="text-align:center">{{ $work->name }}</h4>
            </div>
            @endforeach
            <div style="text-align:center" class="col-sm-6 col-md-2 ">
                <a href="/obras/request">
                    <i style="font-size:180px; color:gray;" class="fa fa-plus-square-o" aria-hidden="true"></i>
                </a>
                <h4 style="text-align:center">Pedir para adicionar</h4>
            </div>
        </div>
    </div>
@endsection