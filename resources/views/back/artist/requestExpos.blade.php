@extends('layout.artist') @section('title','Obra') @section('content')
<div style="width:90%;margin: 3% auto">
    <h2>Agendar marcação</h2>
    <hr>
</div>
<div class="container">
    <form action="/exposicao/storeRequest" method="post">
        {{ csrf_field() }}
        <div class="form-group">
            <label>Nome da exposição</label>
            <input type="text" class="form-control" name="name" > @if ($errors->has('name'))
            <span class="help-block">
                <strong>Insira um nome válido</strong>
            </span>
            @endif
        </div>
        <p>Data Pretendida</p><hr>
        <div class="row">
            <div class="col-md-6 form-group">
                <label>Data Inicio</label>
                <input type="date" class="form-control" name="datewantedinicial" @if (!$errors->has('datewantedinicial')) value="{{ old('datewantedinicial') }}"@endif >
                @if ($errors->has('datewantedinicial'))
                <span class="help-block">
                    <strong>Insira uma data de inicio</strong>
                </span>
                @endif
            </div>
            <div class="col-md-6 form-group">
            <label>Data Final</label>
            <input type="date" class="form-control" name="datewantedfinal" @if (!$errors->has('datewantedfinal')) value="{{ old('datewantedfinal') }}"@endif > 
            @if ($errors->has('datewantedfinal'))
                <span class="help-block">
                    <strong>Insira uma data de fim</strong>
                </span>
                @endif
            </div>
        </div>     
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>
@endsection