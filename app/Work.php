<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Work extends Model
{
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function atype()
    {
        return $this->belongsTo('App\Atype');
    }
    
    public function exposition()
    {
        return $this->belongsTo('App\Exposition');
    }

    public function favorites()
    {
        return $this->hasMany('App\Favorite');
    }

    public function purchases()
    {
        return $this->hasMany('App\Purchase');
    }
}
