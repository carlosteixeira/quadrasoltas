<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'phone', 'address', 'utype_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function utype()
    {
        return $this->belongsTo('App\Utype');
    } 

    public function works()
    {
        return $this->hasMany('App\Work');
    }

    public function markings()
    {
        return $this->hasMany('App\Marking');
    }

    public function purchases()
    {
        return $this->hasMany('App\Purchase');
    }
}
