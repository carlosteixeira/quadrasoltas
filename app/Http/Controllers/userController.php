<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use App\User;
use App\Work;
use App\Exposition;
class userController extends Controller
{

    public function first()
    {
        $user=User::find(Auth::user()->id);
        $user->requestArtist = 0;
        $user->save();
        return redirect ('/home');
    }

    public function obras($id)
    {
        $artista=User::find($id);
        $works= Work::where('is_request', '=', 0)->where('user_id', '=', $id)->get();
        $expos = array();
        foreach($works as $work){
            if($work->exposition_id){
                $exposition=Exposition::find($work->exposition_id);
                if(!in_array($exposition,$expos)){
                    $expos[]=$exposition;
                }   
            }
        }
        return view ('front.obraArtista')->with(compact('artista', 'works', 'expos'));
    }

    public function pendentes()
    {
        if(Auth::user()) {
            if(Auth::user()->utype_id== 1){
                $users=User::where('requestArtist', '=', 1)->orderBy('created_at', 'desc')->get();
                return view ('back.admin.users.pendentes')->with(compact('users'));
            }
        }
        return redirect('/exposicao');
    }

    public function artistas()
    {
        $artistas=User::where('utype_id', '=', 2)->get();
        return view('front.artista')->with(compact('artistas'));
    }


    public function requestUpgrade()
    {
        if(Auth::user()) {
            if(Auth::user()->utype_id== 3){
                $user=User::find(Auth::user()->id);
                $user->requestArtist = 1;
                $user->save();
                request()->session()->flash('sucess', 'O seu pedido para ser Artista vai ser analisado');
                return redirect ('/artista');
            }
        }
        return redirect ('/artista');
    }

    public function upgrade($id)
    {
        if(Auth::user()) {
            if(Auth::user()->utype_id== 1){
                $user=User::findOrFail($id);
                if(strpos(url()->current(),'aceitar')){
                    $user->utype_id= 2;
                    $user->requestArtist = 2;
                    $user->save();
                    request()->session()->flash('sucess', 'O seu pedido de ' . $user->name . ' foi aceite');
                    return redirect('/pendente');
                }
                $user->requestArtist = 2;
                $user->save();
                request()->session()->flash('danger', 'O seu pedido de ' . $user->name . ' não foi aceite');  
                return redirect('/pendente');
            }
        }
        return redirect ('/artista');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()){
            if(Auth::user()->utype_id == 1){
                $users=User::orderBy('created_at', 'desc')->get();
                return view ('back.admin.users.users')->with(compact('users'));

            }
        }
        return view('front.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(Auth::user()){
            if(Auth::user()->utype_id == 1){   
                $user=user::findOrFail($id);
                return view('back.admin.users.udetails')->with(compact('user'));
            }
        }
        return redirect('/');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Auth::user()){
            if(Auth::user()->utype_id == 1){   
                $user=User::findOrFail($id);
                $user->delete();
                request()->session()->flash('sucess','Apagado com Sucesso!');
                return redirect('/user');~
            }
        }
        return redirect('/');
    }
}
