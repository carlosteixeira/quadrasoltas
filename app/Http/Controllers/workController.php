<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Auth;
use App\Work;
use App\Atype;
use App\Purchase;
use App\Favorite;
use App\User;
use App\Exposition;

class workController extends Controller
{
    

    public function comprasResponse($id)
    {
        if(Auth::user()){
            if(Auth::user()->utype_id == 1){
                $compra=Purchase::findOrFail($id);
                if(strpos(url()->current(),'aceitar')){
                    $compra->was_accept = 1;
                    $compra->save();
                    request()->session()->flash('sucess', 'O seu pedido de ' . $compra->user->name . ' foi aceite');
                    return redirect('/comprasPendente');
                }
                $compra->was_accept = 2;
                $compra->save();
                request()->session()->flash('danger', 'O seu pedido de ' . $compra->user->name . ' foi rejeitado');
                return redirect('/comprasPendente');
            }
        }
        return redirect('/compras');
        
    }

    public function pendente()
    {
        if(Auth::user()){
            if(Auth::user()->utype_id == 1){
                $compras=Purchase::where('was_accept', '=', 0)->orderBy('created_at','desc')->get();
                return view('back.admin.obras.compras')->with(compact('compras'));
            }
        }
        return redirect('/compras'); 
    }

    public function allbuys()
    {
        $compras=Purchase::where('user_id', '=', Auth::user()->id)->orderBy('created_at','desc')->get();
        return view('front.compras')->with(compact('compras'));
    }

    public function allfav()
    {
        $favoritos=Favorite::where('user_id', '=', Auth::user()->id)->get();
        return view('front.favorites')->with(compact('favoritos'));
    }

    public function compra($id)
    {
        
            $compra= new Purchase;
            $compra->user_id=Auth::user()->id;
            $compra->work_id=$id;
            $compra->save();
            return redirect('/compras');
    }

    public function fav($id)
    {
        $work=Work::find($id);
        if(strpos(url()->current(),'favoritar')){
            $favorito= new Favorite;
            $favorito->user_id=Auth::user()->id;
            $favorito->work_id=$id;
            $favorito->save();
            return redirect('/favoritos');
        }
        $favorito=Favorite::where('user_id', '=', Auth::user()->id)->where('work_id', '=', $id)->first();
        $favorito->delete();
        return redirect('/favoritos');
    }

    public function pedidos()
    {
        if(Auth::user()){
            if(Auth::user()->utype_id == 1){
                $works=Work::where('is_request', '=', 1)->orderBy('created_at', 'desc')->get();
                return view('back.admin.obras.pedidos')->with(compact('works'));
            }
        }
        return redirect('/compras'); 
    }
    
    public function storeRequest(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'atype' => 'required',
            'price' => 'required',
            'technique' => 'required',
            'workyear' => 'required',
            'dimension' => 'required',
            'photo' => 'required|mimes:jpeg,png'
        ]);

        $work = new Work;
        $work->name =  $request->get('name');
        $work->atype_id =  $request->get('atype');
        $work->user_id = Auth::user()->id;
        $work->technique = $request->get('technique');
        $work->price = $request->get('price');
        $work->workyear = $request->get('workyear');
        $work->dimension = $request->get('dimension');
        $request->photo->storeAs('public/obras/', $request->get('name') .'.'. $request->photo->extension());
        $work->photo = Storage::url('public/obras/' . $request->get('name') .'.'. $request->photo->extension());
        $work->is_request = 1;
        $work->save();
        request()->session()->flash('sucesso', 'O seu pedido foi efetuado com sucesso');
        return redirect('/home')->with(compact(Auth::user()));

    }

    public function request()
    {
        if(Auth::user()){
            if(Auth::user()->utype_id == 2){
                $types=Atype::all();
                return view('back.artist.request')->with(compact('types'));
            }
        }
        return redirect('/artistas'); 

    }

    public function search(Request $request)
    {
        if(Auth::user()){
            if(Auth::user()->utype_id == 1){
                $works=Work::where('name', 'like', '%' . $request->get('name') .  '%')->paginate(9);
                return view('back.admin.obras.allworks')->with(compact('works'));
            }
        }
        return redirect('/artistas'); 

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()){
            if(Auth::user()->utype_id == 1){
                $works=Work::paginate(9);
                return view('back.admin.obras.allworks')->with(compact('works'));
            }
        }
        return redirect('/');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Auth::user()){
            if(Auth::user()->utype_id == 1){
                $expos=Exposition::all();
                $artists=User::where('utype_id', '=', 2)->get();
                $types=Atype::all();
                return view('back.admin.obras.create')->with(compact('types', 'artists', 'expos'));
            }
        }
        return redirect('/');
       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'atype' => 'required',
            'price' => 'required',
            'technique' => 'required',
            'workyear' => 'required',
            'dimension' => 'required',
            'photo' => 'required|mimes:jpeg,png'
        ]);

         $work=new Work;
         $work->name = $request->get('name');
         $work->atype_id = $request->get('atype');
         $work->user_id = $request->get('user');
         $work->technique = $request->get('technique');
         $work->price = $request->get('price');
         $work->workyear = $request->get('workyear');
         $work->dimension = $request->get('dimension');
         $work->exposition_id = $request->get('exposition');
         $request->photo->storeAs('public/obras/', $request->get('name') .'.'. $request->photo->extension());
         $work->photo = Storage::url('obras/' . $request->get('name') .'.'. $request->photo->extension());
         $work->save();
         request()->session()->flash('sucesso', 'Obra inserida com sucesso');
         return redirect('/obras');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $work=Work::findOrFail($id);
        if(Auth::user() && Auth::user()->utype_id == 1){
            if(strpos(url()->current(),'pedido')){
                return view ('back.admin.obras.pedidoshow')->with(compact('work'));
            }
            return view ('back.admin.obras.show')->with(compact('work'));
        }
        return view ('front.obra')->with(compact('work'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Auth::user()){
            if(Auth::user()->utype_id == 1){
                $work=Work::findOrFail($id);
                $work->delete();
                return redirect('/obras/pedidos');
            }
        }
        return redirect ('/');
    }

    public function aceite($id)
    {
        if(Auth::user()){
            if(Auth::user()->utype_id == 1){
                $work=Work::findOrFail($id);
                $work->is_request = 0;
                $work->save();
                return redirect('/obras/pedidos');
            }
        }
        return redirect ('/');
    }
}
