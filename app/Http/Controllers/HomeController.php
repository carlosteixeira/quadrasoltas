<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Work;
use Auth;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::findOrFail(Auth::user()->id);

        switch ($user->utype_id) {
            case 1:
                return view('back.admin.home')->with(compact('user')); 
                break;
            case 2:
                $works=Work::where('user_id', '=', $user->id)->where('is_request', '=', 0)->get();
                return view('back.artist.index')->with(compact('user', 'works')); 
                break;
            default:
            if(strpos(url()->previous(),'expor')){
                return redirect()->action('userController@requestUpgrade');
            }
            if(strpos(url()->previous(),'favoritar')){
                $url= explode("/", url()->previous());
                $id=$url[4];
                return redirect()->action('workController@fav', ['id' => $id]);
            }
            if(strpos(url()->previous(),'pedido')){
                $url= explode("/", url()->previous());
                $id=$url[4];
                $work=Work::find($id);
                return $work;
            }
            return view('front.index')->with(compact('user'));
            
        }
            
        
    }
}
