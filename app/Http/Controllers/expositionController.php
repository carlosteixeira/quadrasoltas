<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use Auth;
use App\Marking;
use App\Exposition;
use DateTime;
use App\Work;
use App\User;
class expositionController extends Controller
{

    public function pedidoShow($id)
    {
        if(Auth::user()){
            if(Auth::user()->utype_id == 1){
                $marking=Marking::findOrFail($id);
                return view('back.admin.exposition.pedidoShow')->with(compact('marking'));
            }
        }
        return redirect('/exposicao');
    }

    public function pedidos()
    {
        if(Auth::user()){
            if(Auth::user()->utype_id == 1){
                $markings=Marking::where('is_request', '=', 0)->orderBy('created_at', 'desc')->get();
                return view('back.admin.exposition.pedidos')->with(compact('markings'));
            }
        }
        return redirect('/exposicao');
    }

    public function storeRequest(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'datewantedinicial' => 'required|date|after:today',
            'datewantedfinal' => 'required|date|after:datewantedinicial'
        ]);
dd();
        $marking = new Marking;
        $marking->name = $request->get('name');
        $marking->user_id = Auth::user()->id;
        $marking->datewantedinicial = $request->get('datewantedinicial');
        $marking->datewantedfinal =  $request->get('datewantedfinal');
        $marking->save();
        request()->session()->flash('sucesso', 'O seu pedido foi efetuado com sucesso');
        return redirect('/exposicao/artista');

    }

    public function request()
    {
        if(Auth::user()){
            if(Auth::user()->utype_id == 2){
                return view('back.artist.requestExpos');
            }
        }
        return redirect('/exposicao');
    }

    public function artista()
    {
        if(Auth::user()){
            if(Auth::user()->utype_id == 2){
                $user=User::find(Auth::user()->id);
                $works=Work::where('user_id', '=', $user->id)->where('is_request', '=', 0)->get();
                $expos = array();
                foreach($works as $work){
                    if($work->exposition_id){
                        $exposition=Exposition::find($work->exposition_id);
                        if(!in_array($exposition,$expos)){
                            $expos[]=$exposition;
                        }   
                    }
                }
                return view('back.artist.expos')->with(compact('user', 'expos')); 
            }
        }
        return redirect('/exposicao');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dates= array();
        $ex = array();
        $expositions=Exposition::orderBy('inicialdate', 'desc')->get();        
        foreach($expositions as $exposition){
          
           $dataS= DateTime::createFromFormat('Y-m-d', $exposition->inicialdate)->format('Y') ;
            if (!in_array($dataS, $dates)) { 
               $dates[$dataS][] =  $exposition;
           }else{
               $dates[$dataS][]+=$exposition;
           }
        }
      if(Auth::user()) {
          if(Auth::user()->utype_id== 1){
            return view ('back.admin.exposition.allexpo')->with(compact('dates'));
          }
      }
        return view ('front.exposicao')->with(compact('dates'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Auth::user()){
            if(Auth::user()->utype_id == 1){
                return view ('back.admin.exposition.create');
            }
        }
        return redirect('/exposicao');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
               'name' => 'required',
               'inicialdate' => 'required|date',
               'finaledate' => 'required|date|after:inicialdate',
               'cover' => 'required|mimes:jpeg,png'
        ]);
            $exposition=new Exposition;
            $exposition->name = $request->get('name');
            $exposition->inicialdate = $request->get('inicialdate');
            $exposition->finaledate = $request->get('finaledate');
            $request->cover->storeAs('public/exposition/', $request->get('name') .'.'. $request->cover->extension());
            $exposition->cover = Storage::url('exposition/' . $request->get('name') .'.'. $request->cover->extension());
            $exposition->save();
            request()->session()->flash('sucesso', 'Inserido com sucesso');
            return redirect('/exposicao');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
        $exposition=Exposition::findOrFail($id);
        if(Auth::user()) {
            if(Auth::user()->utype_id== 1){
                $qtn= Work::where('exposition_id', '=', $exposition->id)->count();
                $works= Work::where('exposition_id', '=', $exposition->id)->get();
                $abc= array();
                foreach ($works as $work) {
                    if($work->user){
                        if(!in_array($work->user->name, $abc)){
                            $abc[]= $work->user->name;
                        }
                    }
                }
              return view ('back.admin.exposition.show')->with(compact('exposition', 'qtn', 'abc'));
            }
        }
        $works=Work::where('exposition_id', '=', $id)->get();
        return view ('front.umaexpo')->with(compact('exposition', 'works'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(Auth::user()) {
            if(Auth::user()->utype_id== 1){
                $exposition=Exposition::findOrFail($id);
                $qtn= Work::where('exposition_id', '=', $exposition->id)->count(); 
                $works= Work::where('exposition_id', '=', $exposition->id)->get();
                $abc= array();
                foreach ($works as $work) {
                    if($work->user){
                        if(!in_array($work->user->name, $abc)){
                            $abc[]= $work->user->name;
                        }
                    }
                }
                return view ('back.admin.exposition.update')->with(compact('exposition', 'qtn', 'abc'));
            }
        }
        return redirect('/exposicao');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(Auth::user()) {
            if(Auth::user()->utype_id== 1){
                $exposition=Exposition::find($id);
                $exposition->name = $request->get('name');
                $exposition->inicialdate = $request->get('inicialdate');
                $exposition->finaledate = $request->get('finaledate');
                $exposition->save();
                request()->session()->flash('sucesso', 'Alteração feita com sucesso');
                return redirect()->action('expositionController@show', ['id' => $id]);
            }
        }
        return redirect('/exposicao');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Auth::user()) {
            if(Auth::user()->utype_id== 1){
                $marking=Marking::findOrFail($id);
                $marking->delete();
                request()->session()->flash('sucesso', 'Pedido removido com sucesso');
                return redirect('/exposicao/pedidos');
            }
        }
        return redirect('/exposicao');
    }

    public function aceite($id)
    {
        if(Auth::user()) {
            if(Auth::user()->utype_id== 1){
                $marking=Marking::findOrFail($id);
                $marking->is_request = 1;
                $marking->save();
                request()->session()->flash('sucesso', 'Pedido aceite com sucesso');
                return redirect('/exposicao/pedidos');
            }
        }
        return redirect('/exposicao');    
    }
}
