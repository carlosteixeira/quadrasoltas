<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use App\Contact;

class contactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()){
            if(Auth::user()->utype_id == 1){
                $contacts=Contact::orderBy('created_at', 'desc')->get();
                return view ('back.admin.contacts.contacts')->with(compact('contacts'));;

            }
        }
        return view('front.contacte');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    } 

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'mail' => 'required',
            'content' => 'required'
     ]);

        $contact = New Contact;
        $contact->name = $request->get('name');
        $contact->mail = $request->get('mail');
        $contact->content = $request->get('content');
        $contact->save();
        request()->session()->flash('sucesso', 'Sua mensagem foi enviada com sucesso');
        return redirect('/contacte');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(Auth::user()){
            if(Auth::user()->utype_id == 1){
                $message=Contact::findOrFail($id);
                $message->is_read = 1 ;
                $message->save();
                return view('back.admin.contacts.message')->with(compact('message'));
            }
        }
        return redirect('/contacte');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Auth::user()){
            if(Auth::user()->utype_id == 1){
                $contact=Contact::findOrFail($id);
                $contact->delete();
                request()->session()->flash('sucesso','Apagado Com Sucesso!');
                return redirect('/contacte');
            }
        }        
        return redirect('/contacte');
    }
}
