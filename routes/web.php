<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('front.index');
});


Route::get('/compra/aceitar/{id}', 'workController@comprasResponse');
Route::get('/compra/rejeitar/{id}', 'workController@comprasResponse');
Route::get('/comprasPendente', 'workController@pendente');
Route::get('/compras', 'workController@allbuys');
Route::get('/favoritos', 'workController@allfav');
Route::get('/obras/{id}/unfav', 'workController@fav');
Route::get('/obras/{id}/favoritar', 'workController@fav');
Route::get('/obras/{id}/pedido', 'workController@compra');
Route::get('/obras/aceitar/{id}', 'workController@aceite');
Route::get('/obras/pedido/{id}', 'workController@show');
Route::get('/obras/pedidos', 'workController@pedidos');
Route::post('/obras/storeRequest', 'workController@storeRequest');
Route::get('/obras/request', 'workController@request');
Route::post('/obras/search', 'workController@search');
Route::resource('/obras', 'workController');

Route::get('/exposicao/pedido/{id}', 'expositionController@pedidoShow');
Route::get('/exposicao/aceitar/{id}', 'expositionController@aceite');
Route::get('/exposicao/pedidos', 'expositionController@pedidos');
Route::post('/exposicao/storeRequest', 'expositionController@storeRequest');
Route::get('/exposicao/request', 'expositionController@request');
Route::get('/exposicao/artista', 'expositionController@artista');
Route::resource('/exposicao', 'expositionController');

Route::get('/first', 'userController@first');
Route::get('/user/rejeitar/{id}', 'userController@upgrade');
Route::get('/user/aceitar/{id}', 'userController@upgrade');
Route::get('/pendente', 'userController@pendentes');
Route::get('/artista/{id}', 'userController@obras');
Route::get('/artista', 'userController@artistas');
Route::get('/expor', 'userController@requestUpgrade');
Route::resource('/user', 'userController');

Route::resource('/contacte', 'contactController');

Auth::routes();

Route::get('/login/expor', function () {
    $rota='/expor';
    return view('auth.login')->with(compact('rota'));
});

Route::get('/login/{id}/pedido', function ($id) {
    $rota='/'.$id.'/pedido';
    return view('auth.login')->with(compact('rota'));
});

Route::get('/login/{id}/favoritar', function ($id) {
    $rota='/'.$id.'/favoritar';
    return view('auth.login')->with(compact('rota'));
});

Route::get('/register/expor', function () {
    $rota='/expor';
    return view('auth.register')->with(compact('rota'));
});
Route::get('/register/{id}/pedido', function ($id) {
    $rota='/'.$id.'/pedido';
    return view('auth.register')->with(compact('rota'));
});

Route::get('/register/{id}/favoritar', function ($id) {
    $rota='/'.$id.'/favoritar';
    return view('auth.register')->with(compact('rota'));
});
Route::get('/home', 'HomeController@index')->name('home');
