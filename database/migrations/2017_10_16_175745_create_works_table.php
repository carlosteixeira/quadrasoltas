<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('works', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('atype_id')->unsigned();
            $table->foreign('atype_id')->references('id')->on('atypes');
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users');
            $table->string('technique');
            $table->string('price');
            $table->integer('workyear');
            $table->string('dimension');
            $table->string('photo');
            $table->integer('exposition_id')->unsigned()->nullable();
            $table->foreign('exposition_id')->references('id')->on('expositions');
            $table->integer('is_request')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('works');
    }
}
