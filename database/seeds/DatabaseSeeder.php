<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
             //utypes
             DB::table('utypes')->insert([
                'name' => 'admin',
            ]);
    
            DB::table('utypes')->insert([
                'name' => 'artist',
            ]);
    
            DB::table('utypes')->insert([
                'name' => 'client',
            ]);

                
            //atypes
            DB::table('atypes')->insert([
                'name' => 'Pintura',
            ]);

            DB::table('atypes')->insert([
                'name' => 'Escultura',
            ]);

            DB::table('atypes')->insert([
                'name' => 'Fotografia',
            ]);

          
            //users

            DB::table('users')->insert([
                'name' => 'admin',
                'email' => 'admin@gmail.com',
                'password' => bcrypt('atec123'),
                'address' => 'rua de cima',
                'phone' => '911911911',
                'utype_id' => 1,
            ]);

            DB::table('users')->insert([
                'name' => 'Aritsta1',
                'email' => 'artista@gmail.com',
                'password' => bcrypt('artista@gmail.com'),
                'address' => 'rua de cima',
                'phone' => '911911911',
                'utype_id' => 2,
            ]);

            DB::table('users')->insert([
                'name' => 'Aritsta2',
                'email' => 'artista2@gmail.com',
                'password' => bcrypt('artista2@gmail.com'),
                'address' => 'rua de cima',
                'phone' => '911911911',
                'utype_id' => 2,
            ]);

            
    
            //expositions
            DB::table('expositions')->insert([
                'name' => 'A primeira exposicao',
                'inicialdate' => '2017-01-01',
                'finaledate' => '2017-02-01',
                'cover' => '/images/diferenca_capa.jpg',
            ]);

           
            
            DB::table('expositions')->insert([
                'name' => 'Segunda exposicao',
                'inicialdate' => '2016-01-01',
                'finaledate' => '2016-02-01',
                'cover' => '/images/improvavel_quimera_capa.jpg',
            ]);
            
    }
}
